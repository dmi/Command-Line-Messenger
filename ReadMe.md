Command-Line-Messenger
======================

SingleContactMessenger is a simple command-line-messenger for one-to-one communication
written by Daniel Missal with (adapted) network code snippets from

[http://beej.us/guide/bgnet/](http://beej.us/guide/bgnet/)

(Author: Brian "Beej Jorgensen" Hall).  
Here, "Single" refers to the fact that in this program, the contact list consists of exactly one person.



COMPILE:
--------

Just use gcc or something similar and compile the file SingleContactMessenger.c like this:

`gcc SingleContactMessenger.c -o SingleContactMessenger`



START:
------

For both server and client, just run the resulting binary file like this:

`./SingleContactMessenger`



USAGE:
------

To find out your global ip-address, use a service like http://www.whatismyip.com .
Remember that the server port you specify must be accessible. You might have to set up some port-forwarding and firewall rules.
