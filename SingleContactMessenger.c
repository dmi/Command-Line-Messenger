// This is a simple single contact messenger written by Daniel Missal with network code snippets from http://beej.us/guide/bgnet/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

void print_dec_byte_content(char * pointer, int length);

int main()
{
	struct SContact
	{
		char ipAddr[16];
		char portNr[6];
	} friend;
	
	char listeningPortNr[6];
	
	int myRole = 0;
	
	int i = 0;
	
	char myCommand[101];
	
	char exitString[5] = "exit";
	char sendString[6] = "send:";
	char receiveString[8] = "receive";
	
	char stringToSend[100];
	int length;
	char lengthAnnouncementString[3];
	
	char stringJustReceived[100];
	
	int status;
	
	struct addrinfo  hints;
	struct addrinfo* servinfo;	// will point to the results
	
	int sockFd;
	
	int socketReuse = 1;
	
	struct sockaddr_storage their_addr;
	socklen_t addr_size;

	int newFd;
	
	int messageFd;
	
	// Basic program setup dialog:
	
	printf("\nWhat do you want to be? Please enter \"1\" for server or \"2\" for client:\n\n");
	
	scanf("%d", &myRole); getchar();
	
	if (myRole == 1) // Server
	{
		// printf("Your IP-adress is %s.\n\n", );
		
		printf("\nPlease enter the number of the port to listen on for incoming connections ( > 1024 ):\n\n");
		
		while ((listeningPortNr[i] = getchar()) != '\n')
		{
			if (i == 5)
			{
				printf("You entered more than 5 characters. Please enter a number between 1025 and 65536!\n");
			
				return 1;
			}
		
			i++;
		}
		
		listeningPortNr[i] = '\0';
	}
	else		// Client
	{
		printf("\nPlease enter the ip-address of your friend's server (e.g. \"192.168.0.12\"):\n\n");
		
		i = 0;
		
		while ((friend.ipAddr[i] = getchar()) != '\n')
		{
			if (i == 15)
			{
				printf("You entered more than 15 characters. Please use the dotted quad notation!\n");
			
				return 1;
			}
		
			i++;
		}
		
		friend.ipAddr[i] = '\0';
		
		printf("\nPlease enter the port number of your friend's server ( > 1024 ):\n\n");
		
		i = 0;
		
		while ((friend.portNr[i] = getchar()) != '\n')
		{
			if (i == 5)
			{
				printf("You entered more than 5 characters. Please enter a number between 1025 and 65536!\n");
			
				return 1;
			}
		
			i++;
		}
		
		friend.portNr[i] = '\0';
	}
	
	// Setting up the connection and connecting:
	
	if (myRole == 1) // Server
	{
		memset(&hints, 0, sizeof hints);	// make sure the struct is empty
		
		hints.ai_family = AF_UNSPEC;		// don't care IPv4 or IPv6
		hints.ai_socktype = SOCK_STREAM;	// TCP stream sockets
		hints.ai_flags = AI_PASSIVE;		// fill in my IP for me
		
		if ((status = getaddrinfo(NULL, listeningPortNr, &hints, &servinfo)) != 0)
		{
			fprintf(stderr, "getaddrinfo error: %s\n", gai_strerror(status));
			
			exit(1);
		}
		
		// servinfo now points to a linked list of 1 or more struct addrinfos
		
		// make a socket:
		
		sockFd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
		
		// allow reuse of this socket when rerunning this program:
		
		setsockopt(sockFd, SOL_SOCKET, SO_REUSEADDR, &socketReuse, sizeof socketReuse);
		
		// bind it to the port we passed in to getaddrinfo():
		
		bind(sockFd, servinfo->ai_addr, servinfo->ai_addrlen);
		
		listen(sockFd, 1);		// now accept an incoming connection:
		
		addr_size = sizeof their_addr;
		
		newFd = accept(sockFd, (struct sockaddr*)&their_addr, &addr_size);
		
		// ready to communicate on socket descriptor new_fd!
		
		freeaddrinfo(servinfo);			// free the linked-list
		
		close(sockFd);
	}
	else		// Client
	{
		memset(&hints, 0, sizeof hints);	// make sure the struct is empty
		
		hints.ai_family = AF_UNSPEC;		// don't care IPv4 or IPv6
		hints.ai_socktype = SOCK_STREAM;	// TCP stream sockets
		
		status = getaddrinfo(friend.ipAddr, friend.portNr, &hints, &servinfo);
		
		// servinfo now points to a linked list of 1 or more struct addrinfos
		
		// make a socket:
		
		sockFd = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
		
		connect(sockFd, servinfo->ai_addr, servinfo->ai_addrlen);
		
		freeaddrinfo(servinfo);			// free the linked-list
	}
	
	// Main messenger loop:
	
	do
	{
		printf("\nPlease enter one of the following commands:\n\n");
		
		printf("send:message (e.g. \"send:Hi! How are you?\" - max. 70 characters)\n");
		printf("receive\n");
		printf("exit\n\n");
		
		i = 0;
		
		while ((myCommand[i] = getchar()) != '\n')
		{
			i++;
			
			if (i == 100)
			{
				printf("\nPlease do not enter more then 100 characters!\n");
			
				return 1;
			}
		}
		
		myCommand[i] = '\0';
		
		if (myRole == 1)
		{
			messageFd = newFd;
		}
		else
		{
			messageFd = sockFd;
		}
		
		if (strcmp(strndup(myCommand, 5), sendString) == 0)	// Send
		{
			for (i = 0, length = 0; (stringToSend[i] = myCommand[i + 5]) != '\0'; i++,length++);
			
			length++;
			
			sprintf(lengthAnnouncementString, "%02d", length);
			
			send(messageFd, lengthAnnouncementString, 3, 0);
			
			send(messageFd, stringToSend, length, 0);
			
			printf("\nYour message has been sent.\n");
		}
		
		if (strcmp(myCommand, receiveString) == 0)		// Receive
		{
			recv(messageFd, stringJustReceived, 3, 0);
			
			length = atoi(stringJustReceived);
			
			recv(messageFd, stringJustReceived, length, 0);
			
			printf("\nReceived message: %s\n", stringJustReceived);
		}
	}
	while(strcmp(myCommand, exitString) != 0);
	
	// Disconnecting:
	
	close(sockFd);
	close(newFd);
	
	return 0;
}

void print_dec_byte_content(char* pointer, int length)
{
	int i;
	
	for (i = 0; i < length; i++)
	{
		printf("\nDecimal (unsigned int) content of pointer[%d] is %d\n", i, (unsigned int) pointer[i]);
	}
}
